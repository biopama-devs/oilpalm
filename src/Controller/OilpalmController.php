<?php
namespace Drupal\biopama_oilpalm\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_klc_ecoregion module.
 */
class OilpalmController extends ControllerBase {
  public function content() {
    $element = array(
	  '#theme' => 'oilpalm'
    );
    return $element;
  }
}