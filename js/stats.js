jQuery(document).ready(function ($) {
  //Initialise map
  // mapboxgl.accessToken = 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg';
  // var mymap = new mapboxgl.Map({
  //   container: 'map-container',
  //   style: 'mapbox://styles/mapbox/satellite-streets-v11',
  //   attributionControl: true,
  //   renderWorldCopies: true,
  //   center: [15, 0],
  //   zoom: 2,
  //   minZoom: 1,
  //   maxZoom: 12,
  // });
  var mymap = $().createMap("map-container", {center: [15, 0], zoom: 2});

  mymap.addControl(new mapboxgl.FullscreenControl());
  mymap.addControl(new mapboxgl.NavigationControl());

  $().addMapLayerBiopamaSources(mymap);
  $().addMapLayer(mymap, "biopamaWDPAPolyJRC");
  $().addMapLayer(mymap, "biopamaWDPAPoint");

  $('.add-layer-button').click(function() {
		var currentLayer = $(this).attr('class').split(' ')[0]; 
		if($(this).hasClass("selected")){
			$(this).html('<i class="fas fa-plus"></i>');
			removeLiveLayer(currentLayer);
			$("."+currentLayer).removeClass("selected");
			$( "img." + currentLayer).removeClass("layer-spinner");
			$( "#mini-loader-wrapper." + currentLayer).remove();
		} else {
			addLiveLayer(currentLayer);
			$(this).addClass("selected");
			$(this).html('<i class="fas fa-minus"></i>');
		}
	});

function addLiveLayer(currentLayer){
		if (mymap.getLayer(currentLayer)){
			console.log("Layer already in the map!")
		} else {
			var layerURL = "";
			var layerScheme = "tms";
			switch(currentLayer) {
			  case "oilpalm":
				  layerURL = 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/oilpalm:oilpalm@EPSG:900913@png/{z}/{x}/{y}.png';
				  break;
			  case "oilpalmMature":
				  layerURL = 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/oilpalm:oilpalm_probability@EPSG:900913@png/{z}/{x}/{y}.png'; 
				  break;
			  case "oilpalmValidation":
				  layerURL = 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/oilpalm:oilpalm_val_points@EPSG:900913@png/{z}/{x}/{y}.png'; 
				  break;
			  default:
				  return;
			}
			mymap.addLayer({
				'id': currentLayer,
				'type': 'raster',
				'source': {
					'id': currentLayer+'-source',
					'type': 'raster',
					'tiles': [layerURL],
					'tileSize': 256,
					'scheme': layerScheme,
				},
				'paint': {}
			}, '');
		}
	}
	function removeLiveLayer(currentLayer){
			if (mymap.getLayer(currentLayer)){
				mymap.removeLayer(currentLayer);
			}
			if (mymap.getSource(currentLayer)){
				mymap.removeSource(currentLayer); 
			}
	}

});